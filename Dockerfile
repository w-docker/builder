ARG PYTHON_VERSION=3.7
ARG ALPINE_VERSION=3.9


FROM alpine:${ALPINE_VERSION} as utils

RUN apk add --no-cache curl

ENV MDB_TOOLS_VERSION=0.7.1
RUN apk add --no-cache glib \
 && apk add --no-cache --virtual .fetch-deps \
      wget \
      libtool \
      glib-dev \
      autoconf \
      automake \
      build-base \
      ca-certificates \
 && mkdir /tmp/mdbtools-dev/ \
 && cd /tmp/mdbtools-dev/ \
 && wget -qO- https://github.com/brianb/mdbtools/archive/${MDB_TOOLS_VERSION}.tar.gz \
    | tar xz \
 && cd /tmp/mdbtools-dev/mdbtools-${MDB_TOOLS_VERSION}/ \
 && autoreconf -i -f \
 && ./configure --disable-man \
 && make \
 && make install \
 && apk del .fetch-deps \
 && rm -rf /tmp/mdbtools-dev/

ENV HUGO_VERSION=0.20.7
RUN apk add --no-cache --virtual .fetch-deps wget tar \
 && wget -qO- https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_Linux-64bit.tar.gz \
    | tar xz hugo -O > /usr/local/bin/hugo \
 && chmod +x /usr/local/bin/hugo \
 && apk del .fetch-deps \
 && /usr/local/bin/hugo version

FROM docker:18.09 as docker


FROM python:${PYTHON_VERSION}-alpine${ALPINE_VERSION}

# PYTHONUNBUFFERED: Force stdin, stdout and stderr to be totally unbuffered.
# PYTHONHASHSEED: Enable hash randomization.
ARG VENV=/opt/venv
ENV PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    VENV=${VENV} \
    PATH=${VENV}/bin:${PATH}

RUN apk add --no-cache glib git sed curl ca-certificates su-exec openssl tar gzip libstdc++

ENV GLIBC_VERSION=2.29-r0
RUN curl -L -o /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub \
 && curl -L -o glibc-${GLIBC_VERSION}.apk https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VERSION}/glibc-${GLIBC_VERSION}.apk \
 && apk add glibc-${GLIBC_VERSION}.apk \
 && rm glibc-${GLIBC_VERSION}.apk

ENV KUBERNETES_VERSION=1.13.5
RUN curl -L -o /usr/local/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl" \
 && chmod +x /usr/local/bin/kubectl \
 && kubectl version --client

ENV HELM_VERSION=2.13.1
RUN curl "https://kubernetes-helm.storage.googleapis.com/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar zx \
 && mv linux-amd64/helm /usr/local/bin/ \
 && helm version --client

RUN addgroup -g 2002 tester \
 && adduser -u 2002 -D -G tester tester

# install global tools
RUN pip install --no-cache-dir setpypi twine

RUN pip install --no-cache-dir virtualenv \
 && virtualenv ${VENV} \
 && pip uninstall virtualenv -y

WORKDIR ${VENV}

RUN pip install --no-cache-dir coverage factory-boy mock setuptools_scm

COPY --from=docker /usr/local/bin/* /usr/local/bin/
COPY --from=utils /usr/local/bin/* /usr/local/bin/
COPY --from=utils /usr/local/lib/libmdb* /usr/local/lib/
